class DomainException(Exception):
    def __init__(self, *args: object) -> None:
        # logger.warning()
        super().__init__(*args)


class RepositoryException(DomainException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ValidationException(DomainException):
    def __init__(self, entity, *args: object) -> None:
        super().__init__(*args)


class NaoGosteiDaCorException(DomainException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class NFEInvalidaException(DomainException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)