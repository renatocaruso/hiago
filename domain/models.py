from __future__ import annotations
from abc import ABC, abstractmethod
from functools import reduce

from typing import List
from uuid import uuid4

from domain.exceptions import NaoGosteiDaCorException, RepositoryException


class AggregateRootInterfaceMarkup(ABC):
    pass


class RepositoryInterface(ABC):

    @abstractmethod
    def save() -> str:
        pass


class ValidadorNFInterface(ABC):
    
    @abstractmethod
    def validar_nf(codigo_nf: str) -> bool:
        pass


class DomainEntityInterface(ABC):

    @abstractmethod
    def validar(self):
        pass

    def salvar(self, respository: RepositoryInterface) -> str:
        try:
            self.validar()
            respository.save(self)
        except Exception as exception:
            raise RepositoryException()


class PlanoPagamento(DomainEntityInterface, AggregateRootInterfaceMarkup):
    id: str
    cobrancas: List[Cobranca]
    nome: str

    def __init__(self) -> None:
        self.cobrancas = []
        self.id = uuid4()

    def __str__(self) -> str:
        return f'{self.id} - {self.nome} - {self.cobrancas}'
    
    def validar(self) -> bool:
        if(self.nome == 'azul'): 
            raise NaoGosteiDaCorException('O nome é da cor que eu nao gosto')
        return True

    def criar_plano_pagamento(self, nome: str, valor:float, repository: RepositoryInterface, validador: ValidadorNFInterface) -> PlanoPagamento:
        plano_pagamento = PlanoPagamento()
        plano_pagamento.nome = nome
        plano_pagamento.valor = valor

        parcela = Parcela(plano_pagamento)

        cobranca = Cobranca([parcela])
        plano_pagamento.cobrancas.append(cobranca)

        if not validador.validar_nf('84237478234'): raise

        plano_pagamento.validar()

        plano_pagamento.salvar(repository)

        return plano_pagamento

    def criar_plano_pagamento_sem_cobranca():
        pass



class Parcela(DomainEntityInterface):
    id: str
    plano_pagamento: PlanoPagamento
    cobrancas: List[Cobranca]

    def __init__(self, plano_pagamento: PlanoPagamento) -> None:
        self.plano_pagamento = plano_pagamento
        self.valor = plano_pagamento.valor

    def validar(self):
        pass



class Cobranca(DomainEntityInterface):
    id: str
    parcelas: List[Parcela]
    valor_total: float

    def __init__(self, parcelas) -> None:
        self.parcelas = parcelas
        self.valor_total = self.somar_parcelas()
    
    def validar(self):
        pass

    def somar_parcelas(self) -> float:
        return reduce(lambda parcela, total: total + parcela.valor, self.parcelas)
