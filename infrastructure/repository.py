from domain.models import RepositoryInterface


class RepositorioFake(RepositoryInterface):
    memoria = set()

    def save(self, entidade):
        print(f'Salvando {entidade}...')
        self.memoria.add(entidade)


class MessageBroker(RepositoryInterface):
    memoria = set()

    def save(self, entidade):
        self.memoria.add(entidade)
