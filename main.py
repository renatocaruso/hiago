from domain.exceptions import NaoGosteiDaCorException
from domain.models import RepositoryInterface
from domain.models import PlanoPagamento
from infrastructure.repository import MessageBroker, RepositorioFake
from infrastructure.validators import ValidadorNFConcreto


repositorio = RepositorioFake()
validador = ValidadorNFConcreto()
# repositorio = MessageBroker()


def consume(repositorio: RepositoryInterface):
    nome = 'azul'
    valor= 200.00

    try:
        plano_pagamento = PlanoPagamento().criar_plano_pagamento(
            nome=nome,
            valor=valor,
            repository=repositorio,
            validador=validador
        )
    except NaoGosteiDaCorException as exception:
        print(f'Erro de aplicação: {exception}')


consume(repositorio)