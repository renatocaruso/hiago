from domain.exceptions import NaoGosteiDaCorException
from domain.models import RepositoryInterface
from domain.models import PlanoPagamento
from infrastructure.repository import MessageBroker, RepositorioFake


# repositorio = RepositorioFake()

repositorio = MessageBroker()


def consume(repositorio: RepositoryInterface):
    nome = input()

    try:
        plano_pagamento = PlanoPagamento().criar_plano_pagamento(nome=nome, repository=repositorio)
    except NaoGosteiDaCorException as exception:
        return 404
